package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {



	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}


	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
		int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}

		}
		return max;
	}


	public int getMin( IntegersBag bag){
		int min=0;
		if(bag!=null)
		{
			Iterator<Integer> iterador = bag.getIterator();
			min = iterador.next();
			int value = iterador.next();
			while(iterador.hasNext())
				value = iterador.next();
			if( value< min)
			{
				min = value;
			}
		}
		return min;
	}
	
	public int getLastOne(IntegersBag bag){
		
		Iterator<Integer> iter = bag.getIterator();
		int value = 0;
		while ( iter.hasNext()) {
			value = iter.next();
		}
		return value;
	}
	
	
	public int getFirstOne(IntegersBag bag){
		return bag.getIterator().next();
	}








}
